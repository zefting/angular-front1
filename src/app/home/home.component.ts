import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor() { }
  cards =
  [
    {title: "All Inclusive Hotels", desc: "All you can eat buffets"},
    {title: "Sunny beaches", desc: "Feel the sand between your toes"},
    {title: "Romantic distinations", desc: "Rekindle your relationship on one of your great distinations"},
    {title: "Native guides", desc: "Get on a authentic guided trip"},
  ]
  ngOnInit(): void {
  }

}
