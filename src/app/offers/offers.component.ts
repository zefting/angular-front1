import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-offers',
  templateUrl: './offers.component.html',
  styleUrls: ['./offers.component.css']
})
export class OffersComponent implements OnInit {

  constructor() { }
  offers= [
    {desc:"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed a ullamcorper augue, vitae porttitor turpis. Vestibulum ac hendrerit neque. Suspendisse maximus hendrerit aliquet. In dignissim id metus et accumsan. Pellentesque turpis ipsum, faucibus a interdum ac, dignissim nec metus. Vestibulum interdum dolor id ligula volutpat, quis gravida turpis fermentum. Mauris dignissim non libero dignissim eleifend. Aenean rhoncus facilisis dui, vel interdum massa cursus a. Cras bibendum, arcu id gravida sodales, arcu metus molestie nulla, eget aliquam nunc orci ultrices mauris.", price:99.99, destination:"Italy"},
    {desc:"Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Etiam luctus leo metus, finibus facilisis risus eleifend at. Phasellus volutpat, dui et dapibus placerat, felis urna dignissim turpis, at pulvinar nisi magna at felis. Donec in egestas sapien. Nunc fermentum pulvinar libero. In porta nisl in commodo auctor. Pellentesque a sapien mauris. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Sed ornare urna et purus ultricies, in posuere augue accumsan. Curabitur at mi tristique tellus consectetur sagittis. Nam luctus volutpat laoreet. Quisque ipsum nulla, commodo in consequat eget, congue sit amet lectus. Sed massa ligula, sagittis et mi at, pharetra ornare mauris.", price:40.99, destination:"Greece"},
    {desc:"Duis placerat luctus est, ac vehicula odio congue in. Phasellus at augue nec dolor vulputate scelerisque. Morbi a elit eu libero venenatis dignissim vitae et neque. Vivamus libero metus, auctor ac porttitor id, vestibulum non massa. Duis eu odio eu purus blandit tempor. Cras congue lacinia elit, eu finibus enim lacinia in. Etiam et tincidunt arcu. Pellentesque sagittis sapien arcu, sit amet lacinia sapien pretium id. Duis fringilla tincidunt lacinia. Nulla urna eros, dapibus in purus at, suscipit tincidunt odio. Aliquam erat volutpat. Mauris venenatis elit quis diam accumsan scelerisque. Proin imperdiet sapien et elementum blandit.", price:50.99, destination:"Spain"},
];
  ngOnInit(): void {
  }

}
