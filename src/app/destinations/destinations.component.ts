import { Component, OnInit } from '@angular/core';

import { filter } from 'rxjs/operators';
// import airportlist
import airport  from './../destinations/airport.json';
import destinations from './../destinations/destinations.json'
import hotels from './../destinations/hotels.json'
@Component({
  selector: 'app-destinations',
  templateUrl: './destinations.component.html',
  styleUrls: ['./destinations.component.css']
})
export class DestinationsComponent implements OnInit {

  constructor() { }

  country: number;
  airport_id:number;
  selectedAirportItem: {airport_id:number, airport_text:string, code:string, city:string};
  selectedDestinationItem: {item_id:number, item_text:string, code:string};
  selectedHotelItem:any;
  desresult:string;
  result: {airport_id: number, airport_text: string, city: string, country:string}[];

  hotels: {HotelId:string, HotelName:string, Description:string, Description_fr:string, Category:string, Tags:any[], ParkingIncluded:string, LastRenovationDate:string, Rating:string, Address:{StreetAddress:string, City:string, StateProvince:string, PostalCode:string, Country:string}, Location:{type:string, coordinates: [number, number]}, Rooms: [{Description: string, Description_fr:string, Type:string, BaseRate:number, BedOptions: string, SleepsCount: number, SmokingAllowed:any, Tags: [string]}]}[] =hotels;
  dropdownList: {item_id:number, item_text:string, code:string}[] =destinations;
  airportDropdownList: {airport_id:number, airport_text:string, city: string, country:string}[]=airport;
  filteredhotels:any[any];
  hotelDropdownSettings = {};
  destinationDropdownSettings = {};
  airportDropdownSettings = {};
  ngOnInit() {

    this.destinationDropdownSettings= {
      idField: 'item_id',
      textField: 'item_text',
      singleSelection: true,
      allowSearchFilter: true,

    };
    this.airportDropdownSettings = {
      idField: 'airport_id',
      textField: 'airport_text',
      singleSelection: true,
      allowSearchFilter: true,

    };
    this.hotelDropdownSettings= {
      idField: 'HotelId',
      textField: 'HotelName',
      singleSelection: true,
      allowSearchFilter: true,

    };
  }
  onDestinationSelect(destination: any) {
    console.log(destination);
    this.country = destination.item_id;
    const desresulttemp = this.dropdownList.filter(countryname => countryname.item_id == this.country)[0];
    this.desresult = desresulttemp.code;
    console.log("dest resultat", this.desresult);
    console.log("country_id",this.country);
    this.selectedDestinationItem = destination;
    this.result = this.airportDropdownList.filter(countryname => countryname.country == this.desresult);
    console.log("resulat air", this.result);
    this.filteredhotels = this.hotels.filter(countryname => countryname.Address.Country == this.desresult);
    console.log("hotels",this.filteredhotels);
  }

  onAirportSelect(airport: any) {
    console.log(airport);
    this.selectedAirportItem = airport;
    this.airport_id = airport.airport_id;
    console.log("airport_id", this.airport_id)
  }
  onHotelSelect(hotel: any) {
    console.log(hotel);
    this.selectedHotelItem = this.hotels.filter(countryname => countryname.HotelId == hotel.HotelId)[0];
    console.log(this.selectedHotelItem);

  }

}
