import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmcServiesComponent } from './emc-servies.component';

describe('EmcServiesComponent', () => {
  let component: EmcServiesComponent;
  let fixture: ComponentFixture<EmcServiesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EmcServiesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EmcServiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
