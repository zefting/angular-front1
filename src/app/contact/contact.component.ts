import { Component, OnInit } from '@angular/core';
import { FormGroup, NgForm } from '@angular/forms';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

  constructor() { }
  contact = {name: "", email: "", message: ""}

  public form: FormGroup;
  ngOnInit(): void {
  }

  submitcontact()
  {
    console.log(this.contact);
  }
}
