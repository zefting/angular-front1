import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AboutComponent } from './about/about.component';
import { ContactComponent } from './contact/contact.component';
import { DestinationsComponent } from './destinations/destinations.component';
import { EmcServiesComponent } from './emc-servies/emc-servies.component';
import { HomeComponent } from './home/home.component';
import { InvestersComponent } from './investers/investers.component';
import { OffersComponent } from './offers/offers.component';
import { PartnersComponent } from './partners/partners.component';
import { PlanesComponent } from './planes/planes.component';

const routes: Routes = [
  {path:"", redirectTo:"/", pathMatch:"full"},
  {path: "", component: HomeComponent},
  {path:"contact", component: ContactComponent},
  {path:"emg-service", component:EmcServiesComponent},
  {path:"about", component:AboutComponent},
  {path:"partners", component:PartnersComponent},
  {path:"investers", component:InvestersComponent},
  {path:"planes", component:PlanesComponent},
  {path:"destinations", component:DestinationsComponent},
  {path: 'offers', component: OffersComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
